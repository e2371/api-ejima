import express from 'express';
const mongoHelper = require('../services/connect.db');
let bodyParser = require('body-parser');
const helmet = require("helmet"); 

const flash = require('express-flash');
const session = require('express-session');
const cors = require('cors');
require('dotenv').config();

const app = express();

app.use(cors());
app.use(helmet());

//middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(flash());
app.use(session({
  secret: <string>process.env.SESSION_SECRET_KEY,
  resave: false,
  saveUninitialized: false,
}));


// Load routes
const router = require('../routes/router');
app.use('/api', router);

// connection à la base de données
mongoHelper.connect();

// set app PORT
const PORT = process.env.PORT || 5000;

// run app
app.listen(PORT, () => console.log("Server listening on port :", PORT) );