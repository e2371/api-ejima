# api-ejima

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```
### Compiles and hot-reloads for development
```
npm run start
```
### Compiles and minifies for production
```
npm run build
```

### Heroku url
https://backend-ejima.herokuapp.com/api/


### Project Description 
Projet test de EJIMA en typescript, avec mongodb
