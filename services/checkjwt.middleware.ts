import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";

export const checkJwt = (req: Request, res: Response, next: NextFunction) => {

  try {
    //Get the jwt token from the head
    let auth = <string>req.headers["authorization"];
    const token = auth.split(' ')[1];

    if(!(token && token.length > 100)){
      res.json({
        status: 401,
        message: "Access denied. Token is required!"
      })
    }

    //Try to validate the token and get data
    const jwtPayload = jwt.verify(token, <string>process.env.JWT_SECRET_KEY, function(err, decoded) {
      if (err) {
        console.log(err);
        return res.json({
          // success: false,
          status: 401,
          message: "Failed to authenticate token.",
        });
      }
      return decoded;
    });

    res.locals.jwtPayload = jwtPayload;

    //The token is valid for 1 hour
    //We want to send a new token on every request
    const { userId } = <any>jwtPayload;
    const newToken = jwt.sign({ userId }, <string>process.env.JWT_SECRET_KEY, {
      expiresIn: "1h"
    });
    res.setHeader("token", newToken);

  } catch (error) {
    //If token is not valid, respond with 401 (unauthorized)
    res.status(401).send();
    return;
  }

  //Call the next middleware or controller
  next();
};

export const verifyToken = (req: Request, res: Response, next: NextFunction) => {
  try {
    //Get the jwt token from the head
    // let auth = <string>req.headers["authorization"];
    const token = <string>req.body.token;

    if(!(token && token.length > 100)){
      res.json({
        status: 401,
        message: "Access denied. Token is required!"
      })
    }

    //Try to validate the token and get data
    const jwtPayload = jwt.verify(token, <string>process.env.JWT_SECRET_KEY, function(err, decoded) {
      if (err) {
        console.log(err);
        return res.json({
          // success: false,
          status: 401,
          message: "Failed to authenticate token.",
        });
      }
      return decoded;
    });

    res.locals.jwtPayload = jwtPayload;

    //The token is valid for 1 hour
    //We want to send a new token on every request
    const { id } = <any>jwtPayload;
    res.setHeader("userId", id);

  } catch (error) {
    //If token is not valid, respond with 401 (unauthorized)
    console.log("Error =>", error);
    return res.json({
      status: 401,
      message: "Veuillez réessayer",
      error: error
    });
  }

  //Call the next middleware or controller
  next();
};

export const generateToken = async (id: string) => {
  const newToken = await jwt.sign({ id }, <string>process.env.JWT_SECRET_KEY, {
    expiresIn: "24h"
  });

  return newToken;
}