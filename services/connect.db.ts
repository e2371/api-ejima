import mongoose from "mongoose";

exports.connect  = async function () {
  const dbUri = <string> process.env.MONGODB_URI

  // We are going to connect our api to mongodb
  return mongoose.connect(dbUri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    })
    .then(() => {
      console.info('Successfully connected to the Mongo Database !');
    })
    .catch((error) => {
      console.error("db connect error", error);
      process.exit(1);
    });

    
}
