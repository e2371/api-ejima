import mongoose from "mongoose";

const UserSchema = new mongoose.Schema(
  {
    email: {
      type: String, 
      unique: true, 
      required: true,
      maxlength: 80
    },
    password: {
      type: String, 
      required: true
    },
    firstname: {
      type: String, 
      maxlength: 60, 
      required: true
    },
    lastname: {
      type: String, 
      maxlength: 40, 
      required: true
    },
    emailValidated: {
      type: Boolean, 
      default: false, 
      required: true
    },
  },
  {
    timestamps: true,
    toObject: {
      virtuals: true
    },
    toJSON: {
      virtuals: true
    }
  }
); 

module.exports = mongoose.model('User', UserSchema);
