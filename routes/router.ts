let express = require('express');

let authController = require('../controllers/auth.controller');
let userController = require('../controllers/user.controller');
let checkJwt = require('../services/checkjwt.middleware');

import { Request, Response } from "express";

const isLogged = checkJwt.checkJwt;
const verifyMyToken = checkJwt.verifyToken;

module.exports = (function() {
  let router = express.Router();

  // Verify if server is running
  router.get('/', (req: Request, res: Response) => res.json({
    message: "Hello AFRICA",
    status: 200
  }));

  // Auth
  router.post('/users/login', authController.userLogin);


  // Users
  router.post('/users/register', userController.register);
  router.get('/users/list', isLogged, userController.list);

  router.post('/users/confirm-email', verifyMyToken, userController.confirmeAccount);
  router.post('/users/forget-password', userController.forgetPassword);
  router.put('/users/reset-password', verifyMyToken, userController.resetPassword);

  // router.get('/users/:id', isLogged, userController.getDetails);
  router.get('/users-count', isLogged, userController.count);

  router.put('/users/update/:id', isLogged, userController.update);
  router.delete('/users/remove/:id', isLogged, userController.delete);


  return router;
})(); 
