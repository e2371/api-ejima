import { Request, Response, NextFunction } from "express";
const User = require('../models/user.model');

const CryptoJS = require("crypto-js");
// const bcrypt = require('bcrypt');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const hashPassword = async function (password: string) {
  // Decrypt password
  const bytes  = CryptoJS.AES.decrypt(password, process.env.PUBLIC_KEY);
  const originalPassword = bytes.toString(CryptoJS.enc.Utf8);
  // console.log("Original password", originalPassword); 

  // Hash password
  var shaSum = await crypto.createHash('sha256');
  await shaSum.update(originalPassword);
  return await shaSum.digest('hex');

  // let hashPassword = '';

  // await bcrypt.hash(originalPassword, 10, function(err: Error, hash: string) {
  //   hashPassword = hash;
  //   console.log("Hash =>", hash);
  //   return hash;
  // });

  // return hashPassword;
}

const verifyLogin = async function (req: Request, res: Response) {

  try {
    //Check if email and password are set
    if (!(req.body.email && req.body.password)) {
      res.json({
        status: 400,
        message: "Veuillez insérer un email et un password",
      })
    }

    //Get user from database
    const encPassword = await hashPassword(req.body.password);
    const user = await User.findOne({ email: req.body.email, password: encPassword }).select('-password -__v') ;

    if(user){
      // Verify if user email address is already confirmed
      if(user.emailValidated == false){
        res.json({
          status: 409,
          message: "Compte non confirmé!"
        });
      } else {
        //Sing JWT, valid for 1 hour
        const token = jwt.sign(
          { userId: user.id },
          process.env.JWT_SECRET_KEY,
          { expiresIn: "1h" }
        );
  
        //Send the jwt and user in the response
        res.json({
          status: 200,
          token: token,
          user: user
        });
      }


    } else {
      // if user not found, return auth fail
      res.json({
        status: 404,
        message: "Email or password incorect"
      })
    }
    

  } catch (error) {
    console.log(error);
    res.json({
      status: 400,
      message: "Error"
    });
  }
  
}


exports.userLogin = function (req: Request, res: Response, next: NextFunction) {
  verifyLogin( req, res);
};

//exports.login = login;
// exports.verifyLoggedUser = passport.authenticate('jwt', { session: false });
exports.hashPassword = hashPassword;