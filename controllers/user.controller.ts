
const User = require('../models/user.model');
const authController = require('./auth.controller');
import { Request, Response } from "express";
const nodemailer = require('nodemailer');
const checkjwt = require('../services/checkjwt.middleware');

// set the mail server params
const mailParams = {
  host: process.env.mailHost,
  port: process.env.mailPORT,
  secure: true,
  auth: {
    user: process.env.mailUser,
    pass: process.env.mailPassword
  },
  // tls: {
  //   rejectUnauthorized: false
  // }
};

// This function insert a new user data in database
exports.register = async function (req: Request, res: Response) {
  const email = req.body.email;  
  const password = req.body.password;  
  const firstname = req.body.firstname; 
  const lastname = req.body.lastname;  

  try {
    // check if all attributes are there
    if(!(email && password && firstname && lastname)){
      return res.json({
        status: 400,
        message: "Entré invalide !"
      });
    }
    // check if the email is already used by another user
    const oldUser = await User.find({email});

    if (oldUser.length != 0) {
      // if the email is already used, send this response
      return res.json({
        status: 409,
        message: "L'email existe déjà !"
      });
      
    } else {
      // if the email is not already used, insert the user data in database
      const new_user = new User({
        email,
        password: await authController.hashPassword(password), // always hash users password
        firstname,
        lastname,
        emailValidated: false
      });

      const savedUser = await new_user.save();

      let confirmEmail = null;

      if(savedUser){
        const token = await checkjwt.generateToken(savedUser._id);
        confirmEmail = await sendConfirmEmail(token, savedUser.email);

        res.json({
          status: 200,
          message: "Utilisateur enrégistré",
          sendmail: confirmEmail
        });
      }
      
    }
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

// This function send a email adrdress confirmation to user
exports.confirmeAccount = async function (req: Request, res: Response) {
  try {
    // Check user by id
    let foundUser = await User.findOne({_id: res.getHeader("userId")});

    if(foundUser){
      // check if email is already confirmed
      if(foundUser.emailValidated && foundUser.emailValidated == true){
        res.json({
          status: 409,
          message: "Votre compte a déjà été confirmé! Vous pouvez vous connecter"
        });
      } else {
        // 
        foundUser.emailValidated = true;
        await foundUser.save();

        res.json({
          status: 200,
          message: "Confirmation effectuée !"
        });
      }

    } else {

      res.json({
        status: 404,
        message: "Votre compte est introuvable! Vérifiez bien le lien de confirmation dans votre boite mail."
      });
    }
    
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

// This function get a user info by id
exports.getDetails = async function(req: Request, res: Response){
  const resourceId = req.params.id;

  try {

    if(resourceId.length > 10){
      const resources = await User.findOne({_id: resourceId}).select('-password -__v');

      res.json({
        status: 200,
        resource: resources,
        newToken: res.getHeader("token") || ''
      });
    } else {
      res.json({
        status: 200,
        message: "Invalid Id"
      });
    }
    
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

// This function count the all user from database
exports.count = async function(req: Request, res: Response) {
  try {
    let total = await User.countDocuments();
    
    res.json({
      status: 200,
      ressource: total,
      newToken: res.getHeader("token") || ''
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

// This function return the users list, by pagination and filters
exports.list = async function (req: Request, res: Response) {
  try {
    // we are set the pagination options
    let page = req.query.page ? parseInt(''+req.query.page) : 0;
    let sort = req.query.sort ? req.query.sort : {"createdAt": -1};
    let perPage = req.query.perPage ? parseInt(''+req.query.perPage) : 50;
    
    let options = {};
    
    // check if there are a search
    if(req.query.filter && JSON.parse(''+req.query.filter).text && JSON.parse(''+req.query.filter).text.length>1){
      options = { 
          $or:[
            {firstname: {$regex: new RegExp(JSON.parse(''+req.query.filter).text, "i") }},
            {lastname: {$regex: new RegExp(JSON.parse(''+req.query.filter).text, "i") }},
            {email: {$regex: new RegExp(JSON.parse(''+req.query.filter).text, "i") }},
          ]
      };
    }
    
    const users = await User.find(options).select('-password -__v')
    .skip(page * perPage) // useful to do a pagination
    .limit(perPage)       // useful to do a pagination
    .sort(sort)           // get the list by DESC
    .lean();

    const database = await User.find(options).countDocuments() ;

    res.json({
      status: 200,
      count: database,
      users: users,
      total: users.length,
      newToken: res.getHeader("token") || ''
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

// This function up to date the user infos
exports.update = async function (req: Request, res: Response) {
  const id = req.params.id;
  try {
    // check user in database
    let foundUser =  await User.findOne({_id: id});

    // If the information is updated, we are up him to date
    foundUser.firstname = req.body.firstname || foundUser.firstname;
    foundUser.lastname = req.body.lastname || foundUser.lastname;
    
    // if password is send, we are hash him befor to save him
    if(req.body.password && req.body.password.length >7){
      foundUser.password = authController.hashPassword(req.body.password);
    }
  
    // Now we can save user infos
    const saveData = await foundUser.save();
    saveData.password = ''; // remove user password befor to send response
    
    res.json({
      user: saveData,
      message: "User is up to date",
      status: 200,
      newToken: res.getHeader("token") || ''
    });
  } catch (err) {
    // When some error is wrong
    console.log(err);
    res.status(400).send(err);
  }
};

// This function check a user by your email and sends to him the password reset link 
exports.forgetPassword = async function (req: Request, res: Response) {
  try {
    // Check user by your email
    let newUser = await User.findOne({email: req.body.email}).select('-password -__v');

    if(newUser){
      const token = await checkjwt.generateToken(newUser._id);

      // send the reset password to user by mail
      const resetPwdCallback = await sendResetPassword(token, newUser.email);

      // send the success response
      res.json({
        status: 200,
        message: "Email envoyé",
        sendmail: resetPwdCallback
      });
    } else {
      // When user not found, we are send a callback 
      res.json({
        status: 404,
        message: "Compte introuvable",
      });
    }
   
  } catch (err) {
    // When some error is wrong
    console.log(err);
    res.status(400).send(err);
  }

};

// This function reset a user password
exports.resetPassword = async function (req: Request, res: Response) {
  const id = res.getHeader("userId");
  try {
    // get the user
    let foundUser = await User.findOne({_id: id});
    // console.log("user data ", foundUser);

    if(!foundUser){
      // if user not found
      res.json({
        status: 404,
        message: "Compte introuvable!"
      });
    } else {

      if(req.body.password){
        // hash the new password
        foundUser.password = await authController.hashPassword(req.body.password);
        // update user password
        await foundUser.save();
        // send the success response
        res.json({
          status: 200,
          message: 'Mot de passe réinitialiser'
        });

      } else {
        // if user password not found in request
        res.json({
          status: 409,
          message: 'Mot de passe introuvable'
        });   
      }        
      
    }

  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

// This function delete user in database
exports.delete = async function (req: Request, res: Response) {
  try {
    if(req.params.id && req.params.id.length > 10){
      // remove user by his id
      await User.findByIdAndRemove(req.params.id);
      res.json({
        status: 200,
        message: "User was deleted.",
        newToken: res.getHeader("token") || ''
      });
    } else {
      res.json({
        status: 400,
        message: "Incorect user Id !"
      });
    }
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }  
};

// This function send a mail confirmation to user 
const sendConfirmEmail = async function(token: string, mail: string) {
  try {

    // init transport for mail sender
    let transporter = nodemailer.createTransport(mailParams);

    // set mail option parameter
    let mailOptions = {
      from: mailParams.auth.user,
      to: mail,
      subject: "Confirmation d'adresse email",
      html: `
      <!DOCTYPE html>
      <html lang="fr">
        <body style="padding: 10%; background-color: #eee; ">
          <div style="background-color: white; padding: 5px; border-color: black; border-width: 2px;">
            <img style="width: 40%; height: auto;"
              src="https://beaudcollection.s3.af-south-1.amazonaws.com/logo.svg" alt="logo EJIMA">
      
            <hr style="color: #78b843; background-color: #78b843;">
            <h4>Bonjour,</h4>
            <p>
              Veuillez cliquer sur le boutton ci-dessous pour confirmer votre adresse email.
              <br> <br>
              <div style=" text-align: center;">
                <a href="https://web-ejima.herokuapp.com/email-verify/`+token+`" target="_blank" style="padding: 12px 8px;
                  background-color: #78b843; color: white; border-radius: 5px; ">
                  Vérifiez mon adresse email
                </a>
              </div>
      
              <br><br><br>
              Si vous n'avez pas créé un compte sur EJIMA, veuillez ignorer ce mail.
              <br><br>
              Coordialement
              <br><br>
              EJIMA
              
            </p>
            <hr>
            <p>
              ejima.com - © Tout droit réservé 2021
       
            </p>
          </div>
        </body>
      </html>
      `,
    };

    // send the mail
    transporter.sendMail(mailOptions, (err: Error, info: JSON) => {
      if(err){
        console.log('Error transporter sendmail :', err);
        return false;
      }
      // console.log('Message sent infos :', info);
    });

    return true;
  } catch (err) {
    console.log(err);
    return false;
  }

};

// This function send a password reset link to user by mail
const sendResetPassword = async function(token: string, mail: string) {
  try {
    // init transport for mail sender
    let transporter = nodemailer.createTransport(mailParams);

    // set mail option parameter
    let mailOptions = {
      from: mailParams.auth.user,
      to: mail,
      subject: "Réinitialisation de mot de passe",
      html: `
      <!DOCTYPE html>
      <html lang="fr">
        <body style="padding: 10%; background-color: #eee; ">
          <div style="background-color: white; padding: 5px; border-color: black; border-width: 2px;">
            <img style="width: 40%; height: auto;"
              src="https://beaudcollection.s3.af-south-1.amazonaws.com/logo.svg" alt="logo EJIMA">
      
            <hr style="color: #78b843; background-color: #78b843;">
            <h4>Bonjour,</h4>
            <p>
              Veuillez cliquer sur le boutton ci-dessous pour réinitialiser votre mot de passe.
              <br> <br>
              <div style=" text-align: center;">
                <a href="https://web-ejima.herokuapp.com/reset-password/`+token+`" target="_blank" style="padding: 12px 8px;
                  background-color: #78b843; color: white; border-radius: 5px; ">
                  Réinitialiser mon mot de passe
                </a>
              </div>
      
              <br><br><br>
              Si vous n'avez pas demandez à récupérer le mot de passe de votre compte sur EJIMA, veuillez ignorer ce mail.
              <br><br>
              Coordialement
              <br><br>
              EJIMA
              
            </p>
            <hr>
            <p>
              ejima.com - © Tout droit réservé 2021
            </p>
          </div>
        </body>
      </html>
      `,
    };

    // send the mail
    transporter.sendMail(mailOptions, (err: Error, info: Response) => {
      if(err){
        console.log('Error transporter sendmail :', err);
        return false;
      }
      // console.log('Message sent infos :', info);
    });

    return true;
  } catch (err) {
    console.log(err);
    return false;
  }

};